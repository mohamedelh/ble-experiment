package com.mohamedelhadiyen.smarthome.blemobileapp;

public final class UUIDcodes {

    public final static String SENSORS_SERVICE = "00010000-9fab-43c8-9231-40f6e305f96d";
    public final static String TEMP_CHAR = "00010001-9fab-43c8-9231-40f6e305f96d";
    public final static String MOTION_CHAR = "00010002-9fab-43c8-9231-40f6e305f96d";
    public final static String DOORSWITCH_CHAR = "00010003-9fab-43c8-9231-40f6e305f96d";
    public final static String DESCRIPTOR = "00002902-0000-1000-8000-00805f9b34fb";


}
