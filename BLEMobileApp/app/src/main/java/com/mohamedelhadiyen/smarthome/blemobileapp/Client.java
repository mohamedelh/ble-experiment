package com.mohamedelhadiyen.smarthome.blemobileapp;

public class Client {

    private String name;
    private String adress;

    public Client() {}

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public String getName() {
        return name;
    }

}

