package com.mohamedelhadiyen.smarthome.blemobileapp;

import com.google.gson.Gson;

import org.json.JSONObject;

public final class MainController {

    private static final MainController SINGLETON;
    private Client client;;
    private Data data;
    static {
        SINGLETON = new MainController();
    }

    public void setValue(String value) {
        Gson g = new Gson();
        data = g.fromJson(value, Data.class);
    }

    public Data getData() {
        return data;
    }


    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public static MainController getSINGLETON() {
        return SINGLETON;
    }

}
