package com.mohamedelhadiyen.smarthome.blemobileapp;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class MainActivity extends AppCompatActivity {

    private BluetoothAdapter adapter;
    private final MainController MAIN_CONTROLLER = MainController.getSINGLETON();
    private Handler mHandler;
    private boolean mScanning;
    private final String DESIRED_DEVICE_ADDRESS = "B8:27:EB:DA:C2:DA";

    private ScanCallback scanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            System.out.println(result.getDevice().getAddress());
            if (result.getDevice().getAddress().equals(DESIRED_DEVICE_ADDRESS)) {
                MAIN_CONTROLLER.getClient().setName(result.getDevice().getName());
                MAIN_CONTROLLER.getClient().setAdress(result.getDevice().getAddress());
                scan(false);
                finish();
                startActivity(new Intent(MainActivity.this, ControlActivity.class));
            }
        }


        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
            System.out.println("SEE HERE PLEASE RESULTS ");
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            System.out.println("SEE HERE PLEASE ERROR TROEP");
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mHandler = new Handler();

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "Not supported", Toast.LENGTH_SHORT).show();
            finish();
        }

        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        adapter = bluetoothManager.getAdapter();

        if (adapter == null) {
            Toast.makeText(this, "Not supported", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

    }


    @Override
    protected void onResume() {
        super.onResume();

        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!adapter.isEnabled()) {
            if (!adapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        } else {
            MAIN_CONTROLLER.setClient(new Client());
            scan(true);
        }
    }

    private void scan(final boolean enable) {

        final BluetoothLeScanner bluetoothLeScanner = adapter.getBluetoothLeScanner();

        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    bluetoothLeScanner.stopScan(scanCallback);
                }
            }, 4000);

            mScanning = true;

            bluetoothLeScanner.startScan(scanCallback);
        } else {

            mScanning = false;
            bluetoothLeScanner.stopScan(scanCallback);
        }


    }
}



