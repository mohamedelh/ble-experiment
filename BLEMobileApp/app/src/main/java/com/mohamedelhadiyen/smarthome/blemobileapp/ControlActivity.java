package com.mohamedelhadiyen.smarthome.blemobileapp;

import android.app.Activity;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.ListView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;


public class ControlActivity  extends Activity {

    private BLEService serviceData;
    private final MainController MAIN_CONTROLLER = MainController.getSINGLETON();
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnected = false;
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    private TextView temperature;
    private TextView motion;
    private TextView door;

    private final ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            serviceData = ((BLEService.LocalBinder) iBinder).getService();

            if (!serviceData.initialize()) {
                finish();
            }

            serviceData.connect(MAIN_CONTROLLER.getClient().getAdress());

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            serviceData = null;
        }

        @Override
        public void onBindingDied(ComponentName name) {

        }

        @Override
        public void onNullBinding(ComponentName name) {

        }
    };

    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (BLEService.ACTION_GATT_CONNECTED.equals(intent.getAction())) {

                mConnected = true;


            } else if (BLEService.ACTION_GATT_DISCONNECTED.equals(intent.getAction())) {

                mConnected = false;

            } else if (BLEService.ACTION_GATT_SERVICES_DISCOVERED.equals(intent.getAction())) {

               displayGattServices(serviceData.getSupportedGattServices());
            } else if (BLEService.ACTION_DATA_AVAILABLE.equals(intent.getAction())) {
                temperature.setText(MAIN_CONTROLLER.getData().getTemperature());
                motion.setText(MAIN_CONTROLLER.getData().getMotion());
                door.setText(MAIN_CONTROLLER.getData().getDoorswitch());
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.control);
        setTitle(MAIN_CONTROLLER.getClient().getAdress());
        temperature = (TextView) findViewById(R.id.temperature);
        motion = (TextView) findViewById(R.id.motion);
        door = (TextView) findViewById(R.id.door);
        bindService(new Intent(this, BLEService.class), serviceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (serviceData != null) {
            final boolean result = serviceData.connect(MAIN_CONTROLLER.getClient().getAdress());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(serviceConnection);
        serviceData = null;
    }


    private void displayGattServices(List<BluetoothGattService> gattServices) {

        if (gattServices == null) return;

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
;
            if (gattService.getUuid().toString().equals(UUIDcodes.SENSORS_SERVICE)) {

                for (BluetoothGattCharacteristic chara : gattService.getCharacteristics()) {

                    serviceData.setCharacteristicNotification(chara, true);

                }
            }
        }

    }


    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BLEService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BLEService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BLEService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BLEService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }


}
