package com.mohamedelhadiyen.smarthome.blemobileapp;

import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("t")
    private int temperature;
    @SerializedName("d")
    private int doorswitch;
    @SerializedName("m")
    private int motion;

    private final String PEOPLE_YES = "Er zijn mensen aanwezig.";
    private final String PEOPLE_NO = "Er zijn geen mensen aanwezig.";
    private final String TEMPERATURE = "De temperatuur is: ";
    private final String DOOR_CLOSED = "De deur is dicht.";
    private final String DOOR_OPEN = "De deur is open.";

    public Data(int temperature, int motion, int doorswitch) {
        this.temperature = temperature;
        this.motion = motion;
        this.doorswitch = doorswitch;
    }

    public void setDoorswitch(int doorswitch) {
        this.doorswitch = doorswitch;
    }

    public void setMotion(int motion) {
        this.motion = motion;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public String getDoorswitch() {
        if (doorswitch == 0) {
            return DOOR_CLOSED;
        } else {
            return DOOR_OPEN;
        }
    }

    public String getMotion() {
        if (motion == 0) {
            return PEOPLE_NO;
        } else {
            return PEOPLE_YES;
        }
    }

    public String getTemperature() {
        return TEMPERATURE + temperature + " C.";
    }

}

