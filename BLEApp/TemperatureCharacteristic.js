const bleno = require("bleno");
const Temperature = require("./Temperature.js");
const SENSOR_SERVICE_UUID = "00010000-9FAB-43C8-9231-40F6E305F96D";
const TEMPERATURE_CHAR_UUID = "00010001-9FAB-43C8-9231-40F6E305F96D";

module.exports = class TemperatureCharacteristic extends bleno.Characteristic {
    constructor() {
        super({
            uuid: TEMPERATURE_CHAR_UUID,
            properties: ["notify"],
            value: null
        });

        this.temp = new Temperature();
    }

    onSubscribe(maxValueSize, updateValueCallback) {
        console.log(`Counter subscribed, max value size is ${maxValueSize}`);
        this.updateValueCallback = updateValueCallback;
    }

    onUnsubscribe() {
        console.log("Counter unsubscribed");
        this.updateValueCallback = null;
    }    

    sendNotification(value) {
        if(this.updateValueCallback) {
            console.log(`Sending notification with value ${value}`);

            const notificationBytes = new Buffer(2);
            notificationBytes.writeInt16LE(value);

            this.updateValueCallback(notificationBytes);
        }
    }

    start() {
        console.log("Starting counter");
        this.handle = setInterval(() => {
            this.sendNotification(this.temp.getTemperature());
        }, 1000);
    }

    stop() {
        console.log("Stopping counter");
        clearInterval(this.handle);
        this.handle = null;
    }
}


