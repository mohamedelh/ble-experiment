const bleno = require('bleno');
const DoorSwitch = require('./DoorSwitch.js');

const SENSORS_SERVICE_UUID = "00010000-9FAB-43C8-9231-40F6E305F96D";
const TEMPERATURE_CHAR_UUID = "00010001-9FAB-43C8-9231-40F6E305F96D";
const MOTION_CHAR_UUID = "00010002-9FAB-43C8-9231-40F6E305F96D";
const SWITCH_CHAR_UUID = "00010003-9FAB-43C8-9231-40F6E305F96D";

module.exports = class DoorSwitchCharacteristic extends bleno.Characteristic {
    constructor() {
        super({
            uuid: SWITCH_CHAR_UUID,
            properties: ["notify"],
            value: null
        });

        this.doorswitch = new DoorSwitch();
    }

    onSubscribe(maxValueSize, updateValueCallback) {
        console.log(`Counter subscribed, max value size is ${maxValueSize}`);
        this.updateValueCallback = updateValueCallback;
    }

    onUnsubscribe() {
        console.log("Counter unsubscribed");
        this.updateValueCallback = null;
    }    

    sendNotification(value) {
        if(this.updateValueCallback) {
            console.log(`Sending notification with value ${value}`);

            const notificationBytes = new Buffer(2);
            notificationBytes.writeInt16LE(value);

            this.updateValueCallback(notificationBytes);
        }
    }

    start() {
        console.log("Starting counter");
        this.handle = setInterval(() => {
            this.counter = (this.counter + 1) % 0xFFFF;
            this.sendNotification(this.doorswitch.check());
        }, 1000);
    }

    stop() {
        console.log("Stopping counter");
        clearInterval(this.handle);
        this.handle = null;
    }
}
