
const { exec } = require('child_process');
const fs = require('fs');
const lineReader = require('line-reader');

BASE_DIR = "/sys/bus/w1/devices/";
     

module.exports = class Temperature {


    constructor() {
      exec("sudo modprobe w1-gpio");
      exec("sudo modprobe w1-therm");
      }

     getTemperature() {
         fs.readdir(BASE_DIR, function(err, items) {
             var device = BASE_DIR.concat(items[0]).concat("/w1_slave");
               lineReader.eachLine(device, function(line, last) {
       if (line.includes("t=")) {
          return parseInt(line.substring(line.indexOf("t=") + 2))/1000;
         }
    });
       });
     }

   }

