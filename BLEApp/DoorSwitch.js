
const Gpio = require('onoff').Gpio;

module.exports = class DoorSwitch {
    constructor() {
	this.door_switch = new Gpio(14, 'in', 'both');
	}
	
    check() {
	return this.door_switch.readSync();
	}
	
}

