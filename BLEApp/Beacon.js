const bleno = require("bleno");
const TemperatureCharacteristic = require("./TemperatureCharacteristic.js");
const MotionCharacteristic = require("./MotionCharacteristic.js");
const DoorSwitchCharacteristic = require("./DoorSwitchCharacteristic.js");

const temperature = new TemperatureCharacteristic();
const motion = new MotionCharacteristic();
const doorswitch = new DoorSwitchCharacteristic();

const SENSORS_SERVICE_UUID = "00010000-9FAB-43C8-9231-40F6E305F96D";
const TEMPERATURE_CHAR_UUID = "00010001-9FAB-43C8-9231-40F6E305F96D";
const MOTION_CHAR_UUID = "00010002-9FAB-43C8-9231-40F6E305F96D";
const SWITCH_CHAR_UUID = "00010003-9FAB-43C8-9231-40F6E305F96D";

temperature.start();
motion.start();
doorswitch.start();

bleno.on("stateChange", state => {

    if (state === "poweredOn") {
        
        bleno.startAdvertising("iBeacon", [SENSORS_SERVICE_UUID], err => {
            if (err) console.log(err);
        });

    } else {
        console.log("Stopping...");
        counter.stop();
        bleno.stopAdvertising();
    }        
});

bleno.on("advertisingStart", err => {

    console.log("Configuring services...");
    
    if(err) {
        console.error(err);
        return;
    }

    let service = new bleno.PrimaryService({
        uuid: SENSORS_SERVICE_UUID,
        characteristics: [temperature, motion, doorswitch]
    });

    bleno.setServices([service], err => {
        if(err)
            console.log(err);
        else
            console.log("Services configured");
    });
});
