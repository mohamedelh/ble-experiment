const Gpio = require('onoff').Gpio;

module.exports = class Motion {

    constructor() {
       this.motion = new Gpio(23, 'in', 'both');
    }

    getMotionInfo() {
        return this.motion.readSync();
    }

}

